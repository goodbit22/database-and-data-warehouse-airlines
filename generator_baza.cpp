#include <random>
#include <string>
#include <fstream>
#include "generator_baza.h"

			void Database::generuj_lotnisko_wyl(){
				uniform_int_distribution<int> licz_naz(0, nazwa.size()-1);
				uniform_int_distribution<int> licz_miej(0, miejscowosc.size()-1);
				random_device rd1("/dev/urandom");
				random_device rd2("/dev/urandom");
				ofstream myfile ("database/file_csv&ctl/lotnisko_wyl.csv");
				for(int i=1; i<=100; i++){
							myfile << i << "," << nazwa[licz_naz(rd1)] << "," << miejscowosc[licz_miej(rd2)]<<endl;	
				}
				myfile.close();

			}

			void Database::generuj_lotnisko_przy(){
				uniform_int_distribution<int> licz_naz(0, nazwa.size()-1);
				uniform_int_distribution<int> licz_miej(0, miejscowosc.size()-1);
				random_device rd1("/dev/urandom");
				random_device rd2("/dev/urandom");
				ofstream myfile ("database/file_csv&ctl/lotnisko_przylot.csv");
				for(int i=1; i<=100; i++){
							myfile << i << "," << nazwa[licz_naz(rd1)] << "," << miejscowosc[licz_miej(rd2)]<<endl;	
				}
				myfile.close();
			}

			void Database::generuj_pracownik(){
				string st_m[] = {"pilot","steward"} ;
				string st_k[] = {"pilot","Stewardesa"};
				int il_el = sizeof(st_m)/sizeof(st_m[0]) -1 ;
				int il_el2 = sizeof(st_k)/sizeof(st_k[0]) -1 ;
				uniform_int_distribution<int> gen_st_m(0,il_el);
				uniform_int_distribution<int> gen_st_k(0,il_el2);
				uniform_int_distribution<int> zakres(0,1);
				uniform_int_distribution<int> licz_mez_im(0,imie_m.size()-1);
				uniform_int_distribution<int> licz_mez_naz(0,nazwisko_m.size()-1);
				uniform_int_distribution<int> licz_kob_im(0,imie_z.size()-1);
				uniform_int_distribution<int> licz_kob_naz(0,nazwisko_z.size()-1);
				uniform_int_distribution<int> licz(1,100);
				random_device rd1("/dev/urandom");
				random_device rd2("/dev/urandom");
				random_device rd3("/dev/urandom");
				ofstream myfile ("database/file_csv&ctl/pracownik.csv");
				for(int i=1; i<=100; i++){
								if (zakres(rd1) == 0){
									myfile << i << "," << imie_m[licz_mez_im(rd2)] << "," << nazwisko_m[licz_mez_naz(rd2)]<<
													","<< st_m[gen_st_m(rd3)]<<"," << licz(rd3)<< endl;
								}
								else{
									myfile << i << "," << imie_z[licz_kob_im(rd2)] << "," << nazwisko_z[licz_kob_naz(rd2)]<<
													","<< st_m[gen_st_k(rd3)]<<"," << licz(rd3)<< endl;
								}
				}
				myfile.close();
			}
			
			void Database::generuj_lot(){
				uniform_int_distribution<int> licznik(1,100);
				uniform_int_distribution<int> person(200,250);
				random_device rd1("/dev/urandom");
				random_device rd2("/dev/urandom");
				random_device rd3("/dev/urandom");
				random_device rd4("/dev/urandom");
				random_device rd5("/dev/urandom");
				ofstream myfile ("database/file_csv&ctl/lot.csv");
							for(int i=1; i<=100; i++){			
								myfile << i << "," << licznik(rd1) << "," <<  licznik(rd2) << "," <<  licznik(rd3) << "," << 
												licznik(rd4) << "," << person(rd5) <<endl;
							}
				myfile.close();

			}

			void Database::generuj_kurs(){
				uniform_int_distribution<int> lokalizacja_ilosc(0,lokalizacja.size()-1);
				random_device rd1("/dev/urandom");
				random_device rd2("/dev/urandom");
				ofstream myfile ("database/file_csv&ctl/kurs.csv");
							for(int i=1; i<=100; i++){			
								myfile << i << "," << lokalizacja[lokalizacja_ilosc(rd1)] << "," << 
												lokalizacja[lokalizacja_ilosc(rd2)] <<endl;
							}
				myfile.close();
			}

			void Database::generuj_klasa_podrozy(){
				string rodzaj_klas[] = {"ekonomiczna", "biznesowa", "pierwsza klasa"};
				int ilosc_elementow = sizeof(rodzaj_klas)/sizeof(rodzaj_klas[0]) -1 ;
				uniform_int_distribution<int> zakres(0,ilosc_elementow);
				random_device rd1("/dev/urandom");
				ofstream myfile ("database/file_csv&ctl/klasa_podrozy.csv");
				for (int i=1; i<=100; i++){
					myfile << i << "," << rodzaj_klas[zakres(rd1)] <<endl;
				}
				myfile.close();
			}
			
				void Database::generuj_klient(){
					uniform_int_distribution<int> zakres(0,1);
					uniform_int_distribution<int> liczba_mez_imie(0,imie_m.size()-1);
					uniform_int_distribution<int> liczba_mez_naz(0,nazwisko_m.size()-1);
					uniform_int_distribution<int> liczba_kob_imie(0,imie_z.size()-1);
				  uniform_int_distribution<int> liczba_kob_naz(0,nazwisko_z.size()-1);
					random_device rd1("/dev/urandom");
					random_device rd2("/dev/urandom");
					ofstream myfile ("database/file_csv&ctl/klient.csv");
					for (int i=1; i<=100; i++){
									if (zakres(rd1) == 0){
										myfile << i << "," << imie_m[liczba_mez_imie(rd2)] << "," << 
														nazwisko_m[liczba_mez_naz(rd2)] <<endl;
									}else{
										myfile << i << "," << imie_z[liczba_kob_imie(rd2)] << "," << 
														nazwisko_z[liczba_kob_naz(rd2)] <<endl;
									}
					}
					myfile.close();
				}
				
				void Database::generuj_samolot(){
					int ilosc_elementow = samolot.size();
					uniform_int_distribution<int> zakres(0,ilosc_elementow-1);
					uniform_int_distribution<int> liczba_miejsc(300,350);
					random_device rd1("/dev/urandom");
					random_device rd2("/dev/urandom");
					ofstream myfile ("database/file_csv&ctl/samolot.csv");
					for (int i=1; i<=100; i++){
						myfile << i << "," << samolot[zakres(rd1)] << "," <<liczba_miejsc(rd2) <<endl;
					}
					myfile.close();
				}

				void Database::generuj_bagaze(){
								string rodzaj_bagazu[] = {"bagaz rejestrowany", "bagaz podreczny", "sprzet sportowy" , "bron"};
								int ilosc_elementow = sizeof(rodzaj_bagazu)/sizeof(rodzaj_bagazu[0]) -1 ;
								uniform_int_distribution<int> zakres(0,ilosc_elementow);
				uniform_int_distribution<int> ilosc(1,8);
				random_device rd1; 
				random_device rd2("/dev/urandom");
				ofstream myfile ("database/file_csv&ctl/bagaze.csv");
				for (int i=1; i<= 10000; i++){
						string bag = rodzaj_bagazu[zakres(rd2)];		
						int ile = ilosc(rd1);
						//cout << "INSERT INTO bagaze VALUES (" << i << ","<< rodzaj_bagazu[zakres(rd2)] << "," << ilosc(rd1) << ")" <<endl ;
						myfile << i << ","<< rodzaj_bagazu[zakres(rd2)] << "," << ilosc(rd1) << endl; 
				}
				myfile.close();
			}
			
			void Database::generuj_bilet(){
				uniform_int_distribution<int> id_bagaz(1,100);
				uniform_int_distribution<int> id_klasa_podrozy(1,100);
				uniform_int_distribution<int> id_lot(1,100);
				uniform_int_distribution<int> id_klient(1,100);
				
				random_device rd1("/dev/urandom"); 
				random_device rd2("/dev/urandom"); 
				random_device rd3("/dev/urandom"); 
				random_device rd4("/dev/urandom"); 

				uniform_int_distribution<int> miejsca_samolocie(1,300);
				uniform_int_distribution<int> cena(400,950);
				
				random_device rd5("/dev/urandom"); 
				random_device rd6("/dev/urandom"); 

				string godzina_odlotu;
				string godzina_przylotu;

				uniform_int_distribution<int> godzina(1,24);
				uniform_int_distribution<int> minuta(0,59);
				
				random_device rd7("/dev/urandom"); 
				random_device rd8("/dev/urandom"); 
				random_device rd9("/dev/urandom"); 
				random_device rd10("/dev/urandom"); 
				
				string data_przylotu;
				string data_odlotu;
				
				random_device rd11("/dev/urandom"); 
				random_device rd12("/dev/urandom"); 
				random_device rd13("/dev/urandom"); 
				random_device rd14("/dev/urandom"); 
				
				uniform_int_distribution<int> miesiac(1,12);
				uniform_int_distribution<int> dzien(1,28);
				
				ofstream myfile;
				myfile.open("database/file_csv&ctl/bilet.csv");
				for(int i=1; i<=10000; i++ ){
						int bag = id_bagaz(rd1);
					  int klasa = id_klasa_podrozy(rd2);
				  	int lot = id_lot(rd3);
						int klient = id_klient(rd4);

						int godzina1 = godzina(rd7);
						int minuta1 = minuta(rd8);

						if(godzina1 < 10){

							godzina_odlotu = "0"+ to_string(godzina1) + ":" + to_string(minuta1);	
						}		
						else if(minuta1 < 10 ){

							godzina_odlotu =  to_string(godzina1) + ":" +"0" + to_string(minuta1);	
						}
						else if(godzina1 < 10  && minuta1 < 10){

							godzina_odlotu =  "0" + to_string(godzina1) + ":" +"0" + to_string(minuta1);	
						}
						else {

							godzina_odlotu = to_string(godzina(rd7)) + ":" + to_string(minuta(rd8));	
						}

						godzina_odlotu = to_string(godzina(rd7)) + ":" + to_string(minuta(rd8));	
						godzina_przylotu = to_string(godzina(rd9)) + ':' + to_string(minuta(rd10));
						int miesiac1= miesiac(rd11);
						int dzien1	= dzien(rd12);
						if(dzien1 < 10 && miesiac1 < 10 ){
								data_odlotu = "2020/0"   + to_string(miesiac1)  + "/0" + to_string(dzien1);  
								//data_odlotu = "0" + to_string(dzien1) + "/0"+ to_string(miesiac1) +"/2020";  
						}
						else if(miesiac1 < 10 ){
							
								data_odlotu = "2020/0"   + to_string(miesiac1)  + "/" + to_string(dzien1);  
								//data_odlotu = to_string(dzien1) + "/0"+ to_string(miesiac1) +"/2020";  
						}
						else if(dzien1 < 10){
								data_odlotu = "2020/"   + to_string(miesiac1)  + "/0" + to_string(dzien1);  
								//data_odlotu = "0" + to_string(dzien1) + "/"+ to_string(miesiac1) +"/2020"; 

						}
						else{
							data_odlotu = "2020/"  +  to_string(miesiac1)   + "/" + to_string(dzien1);  
							//data_odlotu =  to_string(dzien1) + "/"+ to_string(miesiac1) +"/2020"; 
						}

						int miesiac2 = miesiac(rd13);
						int dzien2 = dzien(rd14);
						if(miesiac2 < 10 && dzien2 < 10){
								data_przylotu = "2020/0"  +  to_string(miesiac2)   + "/0" + to_string(dzien2);   
								//data_przylotu = "0" + to_string(dzien2) + "/0" + to_string(miesiac2) + "/2020";
						}
						else if(miesiac2 < 10 ){
							data_przylotu = "2020/0"  +  to_string(miesiac2)   + "/" + to_string(dzien2);   
								//data_przylotu =  to_string(dzien2) + "/0" + to_string(miesiac2) + "/2020";
						}
						else if(dzien2 < 10 ){
								data_przylotu = "2020/"  +  to_string(miesiac2)   + "/0" + to_string(dzien2);   
								//data_przylotu = "0" + to_string(dzien2) + "/" + to_string(miesiac2) + "/2020";
						}
						else{
							data_przylotu = "2020/"  +  to_string(miesiac2)   + "/" + to_string(dzien2);   
								//data_przylotu = to_string(dzien2) + "/" + to_string(miesiac2) + "/2020";
						}

						int miejsc = miejsca_samolocie(rd5);
						int cen_los = cena(rd6);
						
						myfile << i << "," << bag <<"," << klasa << ","<< lot << "," << klient
									 << "," << godzina_odlotu << "," << godzina_przylotu
								 	 << "," << data_odlotu << "," << data_przylotu
									 << "," << miejsc << ","	<< cen_los<<endl; 
				}
				myfile.close();
		}
			void Database::przypis_wartosc(){

				ifstream myfile1 ("file_txt/imiona_m.txt");
				ifstream myfile2 ("file_txt/imiona_k.txt");
				ifstream myfile3 ("file_txt/nazwiska_k.txt");
				ifstream myfile4 ("file_txt/nazwisko_m.txt");
				ifstream myfile5 ("file_txt/miejscowosc.txt");
				ifstream myfile6 ("file_txt/nazwa.txt");
				ifstream myfile7 ("file_txt/samolot.txt");
				ifstream myfile9 ("file_txt/lokalizacja.txt");


				string line;
				int i=0;
				if(myfile1.is_open()){
								while(getline(myfile1,line)){
												imie_m.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile1.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}

				i=0;
				if(myfile2.is_open()){
								while(getline(myfile2,line)){
												imie_z.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile2.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}
				
				i=0;
				if(myfile3.is_open()){
								while(getline(myfile3,line)){
												nazwisko_m.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile3.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}
		
				
				i=0;
				if(myfile4.is_open()){
								while(getline(myfile4,line)){
												nazwisko_z.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile4.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}
				
				i=0;
				if(myfile5.is_open()){
								while(getline(myfile5,line)){
												miejscowosc.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile5.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}
				
				i=0;
				if(myfile6.is_open()){
								while(getline(myfile6,line)){
											nazwa.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile6.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}
				
				i=0;
				if(myfile7.is_open()){
								while(getline(myfile7,line)){
											samolot.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile7.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}

				i=0;
				if(myfile9.is_open()){
								while(getline(myfile9,line)){
											lokalizacja.insert(pair<int,string>(i,line));
												i++;		
								}
								myfile9.close();
				}else{
								cout << "nie udalo sie odczytac pliku "<<endl;
				}
				
}



