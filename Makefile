G = g++

all: output

main.o: main.cpp 
	$(G) -c  main.cpp 

generator_baza.o: generator_baza.cpp generator_baza.h
	$(G) -c  generator_baza.cpp   

output: main.o generator_baza.o
	$(G) main.o generator_baza.o -o main

clean:
	rm -f main
	rm *.o
