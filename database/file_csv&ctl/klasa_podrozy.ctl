LOAD DATA
INFILE 'klasa_podrozy.csv'
BADFILE 'klasa_podrozy.bad'
DISCARDFILE 'klasa_podrozy.dsc'
REPLACE INTO TABLE klasa_podrozy
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_klasa_podrozy,nazwa)