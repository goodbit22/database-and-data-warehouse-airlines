LOAD DATA
INFILE 'klient.csv'
BADFILE 'klient.bad'
DISCARDFILE 'klient.dsc'
REPLACE INTO TABLE klient
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_klient,imie,nazwisko)