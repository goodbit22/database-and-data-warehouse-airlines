LOAD DATA
INFILE 'lot.csv'
BADFILE 'lot.bad'
DISCARDFILE 'lot.dsc'
REPLACE INTO TABLE lot
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_lot,id_lotnisko_wyl,id_lotnisko_przylot,id_samolot,id_kurs,ilosc_osob)