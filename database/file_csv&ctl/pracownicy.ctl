LOAD DATA
INFILE 'pracownicy.csv'
BADFILE 'pracownicy.bad'
DISCARDFILE 'pracownicy.dsc'
REPLACE INTO TABLE pracownicy
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_pracownika,imie,nazwisko,stanowisko,id_samolot)
