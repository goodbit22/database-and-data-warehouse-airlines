LOAD DATA
INFILE 'bilet.csv'
BADFILE 'bilet.bad'
DISCARDFILE 'bilet.dsc'
REPLACE INTO TABLE bilet
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_biletu,id_bagaz,id_klasa_podrozy,id_lot,id_klienta,godzina_odlotu,minuta_odlotu,godzina_przylotu,minuta_przylotu,data_odlotu "to_date(:data_odlotu,'yyyy/mm/dd')",data_przylotu "to_date(:data_przylotu,'yyyy/mm/dd')",miejce_w_samolocie,cena)
