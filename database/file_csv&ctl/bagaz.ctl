LOAD DATA
INFILE 'bagaze.csv'
BADFILE 'bagaze.bad'
DISCARDFILE 'bagaze.dsc'
REPLACE INTO TABLE bagaze
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_bagaze,rodzaj_bagazu,ilosc_bagazu)