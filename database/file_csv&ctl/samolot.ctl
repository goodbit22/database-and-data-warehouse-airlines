LOAD DATA
INFILE 'samolot.csv'
BADFILE 'samolot.bad'
DISCARDFILE 'samolot.dsc'
REPLACE INTO TABLE samolot
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_samolot,model_samolotu,ilosc_miejsc)
