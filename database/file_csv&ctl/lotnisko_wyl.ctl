LOAD DATA
INFILE 'lotnisko_wyl.csv'
BADFILE 'lotnisko_wyl.bad'
DISCARDFILE 'lotnisko_wyl.dsc'
REPLACE INTO TABLE lotnisko_wyl
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_lotnisko_wyl,nazwa,miejscowosc)