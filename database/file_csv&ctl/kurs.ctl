LOAD DATA
INFILE 'kurs.csv'
BADFILE 'kurs.bad'
DISCARDFILE 'kurs.dsc'
REPLACE INTO TABLE kurs
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(id_kurs,lokalizacja_od,lokalizacja_do)
