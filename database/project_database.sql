drop table pojazdy cascade constraints;
drop table klienci cascade constraints;
drop table Miasta cascade constraints;
drop table rodzaj_uslugi cascade constraints;
drop table Uslugi cascade constraints;
drop table typy_platnosci cascade constraints;
drop table pracownicy cascade constraints;
drop table Myjnia cascade constraints;
drop table Sprzedaz cascade constraints;

Create table pojazdy(
    id_pojazdu number(15,0) not null CONSTRAINT pojazd_pk Primary key ,
    model_samochodu varchar(30) not null,
    typ_pojazdu  varchar(30) not null
);
Create table klienci(
    id_klienta number(15,0) not null CONSTRAINT klient_pk Primary key ,
    id_pojazdu number(15,0) NOT NULL CONSTRAINT pojazd_fk REFERENCES pojazdy(id_pojazdu),
    imie varchar2(20)  not null,
    nazwisko varchar2(20)  not null
);
Create table Miasta(
    id_miasta number(15,0) not null CONSTRAINT miasta_pk Primary key,
    nazwa_miasta varchar2(30) not null,
    nazwa_wojewodztwa  varchar2(30) not null
);
Create table rodzaj_uslugi(
    id_rodzaj_uslugi number(15,0) not null CONSTRAINT rodzaj_uslugi_pk Primary key,
    program_1 integer not null,
    program_2 integer not null,
    program_3 integer not null,
    program_4 integer not null
);
Create table Uslugi(
    id_uslugi  number(15,0) not null CONSTRAINT usluga_pk Primary key,
    id_rodzaj_uslugi number(15,0) NOT NULL CONSTRAINT rodzaj_uslugi_fk REFERENCES rodzaj_uslugi(id_rodzaj_uslugi)
);
Create table typy_platnosci(
    id_typ_platnosci  number(15,0) not null CONSTRAINT typ_platnosci_pk Primary key,
    typ_platnosci varchar2(30) not null
);
create table pracownicy(
    id_pracownika  number(15,0) not null CONSTRAINT pracownik_pk Primary key,
    imie varchar2(30) not null,
    nazwisko varchar2(30) not null,
    stanowisko varchar2(30) not null
);
create table Myjnia (
    id_myjnia number(15,0) not null CONSTRAINT myjnia_pk Primary key,
    id_miasta number(15,0) NOT NULL CONSTRAINT miasta_fk REFERENCES Miasta(id_miasta),
    id_pracownika  number(15,0) NOT NULL CONSTRAINT praoownik_fk REFERENCES pracownicy(id_pracownika),
    adres varchar2(40) not null 
);
create table Sprzedaz(
    id_sprzedaz number(15,0) not null CONSTRAINT sprzedaz_pk Primary key,
    id_klienta number(15,0) NOT NULL CONSTRAINT klient_fk REFERENCES klienci(id_klienta),
    id_myjnia number(15,0) NOT NULL CONSTRAINT myjnia_fk REFERENCES myjnia(id_myjnia),
    id_uslugi  number(15,0) NOT NULL CONSTRAINT uslugi_fk REFERENCES Uslugi(id_uslugi),
    id_typ_platnosci number(15,0) NOT NULL CONSTRAINT typ_platnosci_fk REFERENCES typy_platnosci(id_typ_platnosci),
    data_sprzedazy date not null, 
    czas_uslugi  number(2,0) not null,
    czas_rozpoczecia number(2,0) not null,
    czas_zakoczenia number(2,0) not null,
    numer_karty number(15,0) not null,
    cena number(7,2) not null,
    numer_stanowiska  number(1) not null,
    numer_parahonu number(10) not null
);



