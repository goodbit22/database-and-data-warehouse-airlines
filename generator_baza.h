#ifndef GEN_BAZ
	#define GEN_BAZ 
	#include <map>
	#include <iostream>
	using namespace std;
class Database {
				private:
			map <int,string> imie_m;
			map <int,string> imie_z;
			map <int,string> nazwisko_m;
			map <int,string> nazwisko_z;
			map <int,string> miejscowosc;
			map <int,string> nazwa;
			map <int,string> samolot;
			map <int,string> lokalizacja;
				public:
	void przypis_wartosc();
  void generuj_bagaze();
  void generuj_samolot();
	void generuj_bilet();
  void generuj_klient();
  void generuj_klasa_podrozy();
  void generuj_kurs();
  void generuj_lot();
  void generuj_pracownik();
 	void generuj_lotnisko_przy();
  void generuj_lotnisko_wyl();
};
#endif
