drop table lokalizacja_od CASCADE CONSTRAINTS;
drop table lokalizacja_do CASCADE CONSTRAINTS;
drop table czas_odlotu CASCADE CONSTRAINTS;
drop table czas_przylotu CASCADE CONSTRAINTS;
drop table czas CASCADE CONSTRAINTS;
drop table klient CASCADE CONSTRAINTS;
drop table samolot CASCADE CONSTRAINTS;
drop table pracownicy CASCADE CONSTRAINTS;
drop table stanowisko CASCADE CONSTRAINTS;
drop table data_odlotu CASCADE CONSTRAINTS;
drop table  data_przylotu CASCADE CONSTRAINTS;
drop table  data CASCADE CONSTRAINTS;
drop table  lotnisko_wyl CASCADE CONSTRAINTS;
drop table lotnisko_przylot CASCADE CONSTRAINTS;
drop table lotnisko CASCADE CONSTRAINTS;
drop table lot CASCADE CONSTRAINTS;
drop table bilet CASCADE CONSTRAINTS;



create table lokalizacja_od(
    id_lokalizacja_od  number(15,0) NOT NULL CONSTRAINT lokalizacja_od_pk PRIMARY KEY,
    nazwa varchar2(50) NOT NULL
);
create table lokalizacja_do(
    id_lokalizacja_do  number(15,0) NOT NULL CONSTRAINT lokalizacja_do_pk PRIMARY KEY,
    nazwa varchar2(50) NOT NULL
);

create table czas_odlotu (
    id_czas_odlotu number(15,0) NOT NULL CONSTRAINT czas_odlotu_pk PRIMARY KEY,
    godzina number(2,0) NOT NULL,
    minuta number(2,0) NOT NULL, 
    CHECK( godzina < 25 AND minuta < 60 )	
);

create table czas_przylotu (
    id_czas_przylotu number(15,0) NOT NULL CONSTRAINT czas_przylotu_pk PRIMARY KEY,
    godzina number(2,0) NOT NULL,
    minuta number(2,0) NOT NULL,
    CHECK( godzina < 25 AND minuta < 60 )	
);

create table czas (
    id_czas number(15,0) NOT NULL CONSTRAINT czas_pk PRIMARY KEY,
    id_czas_odlotu number(15,0) NOT NULL CONSTRAINT czas_odlotu_fk REFERENCES czas_odlotu(id_czas_odlotu),
    id_czas_przylotu number(15,0) NOT NULL CONSTRAINT czas_przylotu_fk REFERENCES czas_przylotu(id_czas_przylotu)
);

create table klient (
    id_klient number(15,0) NOT NULL CONSTRAINT klient_pk PRIMARY KEY,
    imie varchar2(25) NOT NULL,
    nazwisko varchar2(25) NOT NULL
);

create table samolot (
    id_samolot  number(15,0) NOT NULL CONSTRAINT samolot_pk PRIMARY KEY,
    model varchar2(30) NOT NULL,
    ilosc_miejsc number(3,0) NOT NULL,
    CHECK (ilosc_miejsc = 300)
);
create table stanowisko (
    id_stanowiska  number(15,0) NOT NULL CONSTRAINT stanowisko_pk PRIMARY KEY,
    nazwa varchar2(30) NOT NULL
);
create table pracownicy (
    id_pracownika  number(15,0) NOT NULL CONSTRAINT pracownicy_pk PRIMARY KEY,
    imie varchar2(30) NOT NULL,
    nazwisko varchar2(30) NOT NULL,
    id_stanowiska number(15,0) NOT NULL CONSTRAINT stanowisko_fk REFERENCES stanowisko(id_stanowiska),
    id_samolot number(15,0) NOT NULL CONSTRAINT samolot_fk REFERENCES samolot(id_samolot)
);

create table data_odlotu(
    id_data_odlotu number(15,0) NOT NULL CONSTRAINT data_odlotu_pk PRIMARY KEY,
    dzien  number(4,0) NOT NULL,
    miesiac number(2,0) NOT NULL,
    kwartal varchar2(15) NOT NULL,
    rok number(4,0) NOT NULL
);

create table data_przylotu(
    id_data_przylotu number(15,0) NOT NULL CONSTRAINT data_przylotu_pk PRIMARY KEY,
    dzien  number(4,0) NOT NULL,
    miesiac number(2,0) NOT NULL,
    kwartal varchar2(15) NOT NULL,
    rok number(4,0) NOT NULL
);

create table data(
     id_data number(15,0) NOT NULL CONSTRAINT data_pk PRIMARY KEY,
     id_data_odlotu number(15,0) NOT NULL CONSTRAINT data_odlotu_fk REFERENCES data_odlotu(id_data_odlotu),
     id_data_przylotu number(15,0) NOT NULL CONSTRAINT data_przylotu_fk REFERENCES data_przylotu(id_data_przylotu)
);

create table lotnisko_wyl (
    Id_lotnisko_wyl number(15,0) NOT NULL CONSTRAINT lotnisko_wyl_pk Primary KEY,
    nazwa  varchar2(40) NOT NULL,
    miejscowosc varchar2(40) NOT NULL
);

create table lotnisko_przylot (
    Id_lotnisko_przylot number(15,0) NOT NULL CONSTRAINT lotnisko_przylot_pk Primary KEY,
    nazwa  varchar2(40) NOT NULL,
    miejscowosc varchar2(40) NOT NULL
);

create table lotnisko (
    id_lotnisko number(15,0) NOT NULL CONSTRAINT lotnisko_pk Primary KEY,
    Id_lotnisko_wyl  number(15,0) NOT NULL CONSTRAINT lotnisko_wyl_fk REFERENCES lotnisko_wyl(Id_lotnisko_wyl),
    Id_lotnisko_przylot number(15,0) NOT NULL CONSTRAINT lotnisko_przylot_fk REFERENCES lotnisko_przylot(Id_lotnisko_przylot)
);
create table lot (
    id_lot number(15,0) NOT NULL CONSTRAINT lot_pk Primary KEY,
    ilosc_osob number(15,0)  NOT NULL
);

create table bilet (
    id_biletu number(15,0) NOT NULL CONSTRAINT bilet_pk PRIMARY KEY,
    id_klienta number(15,0) NOT NULL CONSTRAINT klient_fk REFERENCES klient(id_klient),
    id_lotnisko number(15,0) NOT NULL CONSTRAINT lotnisko_fk REFERENCES lotnisko(id_lotnisko),
    id_samolot number(15,0) NOT NULL CONSTRAINT samolot_fk2 REFERENCES samolot(id_samolot),
    id_lokalizacja_od  number(15,0) NOT NULL CONSTRAINT lokalizacja_od_fk REFERENCES lokalizacja_od(id_lokalizacja_od),
    id_lokalizacja_do  number(15,0) NOT NULL CONSTRAINT lokalizacja_do_fk REFERENCES lokalizacja_do(id_lokalizacja_do),
    id_lot number(15,0) NOT NULL CONSTRAINT lot_fk REFERENCES lot(id_lot),
    id_data number(15,0) NOT NULL CONSTRAINT data_fk REFERENCES data(id_data),
    id_czas number(15,0) NOT NULL CONSTRAINT czas_fk REFERENCES czas(id_czas),
    rodzaj_bagazu varchar2(30) NOT NULL,
    ilosc_bagazu number(2,0),
    miejsce_w_samolocie number(3,0) NOT NULL,
    cena number(15,0) NOT NULL,
    CHECK (ilosc_bagazu <= 10)
);
