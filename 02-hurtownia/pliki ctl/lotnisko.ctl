LOAD DATA
INFILE 'lotnisko.csv'
BADFILE 'lotnisko.bad'
DISCARDFILE 'lotnisko.dsc'
REPLACE INTO TABLE lotnisko
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(ID_LOTNISKO,ID_LOTNISKO_WYL,ID_LOTNISKO_PRZYLOT)