LOAD DATA
INFILE 'samolot.csv'
BADFILE 'samolot.bad'
DISCARDFILE 'samolot.dsc'
REPLACE INTO TABLE samolot
FIELDS TERMINATED BY ","
TRAILING NULLCOLS
(ID_SAMOLOT,MODEL,ILOSC_MIEJSC)